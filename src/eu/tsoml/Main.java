package eu.tsoml;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        Book book1 = new Book("The Last Wish", "Andrzej Sapkowski", 1993);
        Book book2 = new Book("Sword of Destiny", "Andrzej Sapkowski", 1992);
        Book book3 = new Book("The 10X Rule", "Grant Cardone", 2011);
        Book book4 = new Book("Before the Storm", "Christie Golden", 2018);

        Book[] arrayOfBooks = {book1, book2, book3, book4};

        System.out.println("Task 1: Oldest book");

        int oldestBookId = 0;

        for (int i = 1; i < arrayOfBooks.length; i++) {
            if (arrayOfBooks[i].yearOfPublication < arrayOfBooks[oldestBookId].yearOfPublication) {
                oldestBookId = i;
            }
        }

        System.out.println("Oldest Book by: " + arrayOfBooks[oldestBookId].author);

        System.out.println("\nTask 2: Search by author");
        System.out.print("Enter author: ");

        String searchAuthor = scan.nextLine();
        boolean isAvailable = false;

        for (Book arrayOfBook : arrayOfBooks) {
            if (arrayOfBook.author.equals(searchAuthor)) {
                System.out.println(arrayOfBook.title);
                isAvailable = true;
            }
        }

        if (!isAvailable) {
            System.out.println(searchAuthor + " books not found");
        }


        System.out.println("\nTask 3: max year");
        System.out.print("Enter year: ");

        int maxYear = Integer.parseInt(scan.nextLine());

        for (Book arrayOfBook : arrayOfBooks) {
            if (arrayOfBook.yearOfPublication < maxYear) {
                System.out.println(arrayOfBook.title + " " + arrayOfBook.author + " " + arrayOfBook.yearOfPublication);
            }
        }

    }
}
